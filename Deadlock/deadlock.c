// Cyrus Duncan CST-221 10/12/2020
// includes
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
// Mutex lock and initialize mutex
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
/*
* Method to place a call to the president
* Caller parameter to see who is calling
*/
void *call(void *caller) {
// create variable to set the charater and parse it to equal the parameter
    char *s = caller;
// For loop of president answering each caller
    for (int i = 0; i < 4; ++i) {
// Lock the mutex
        pthread_mutex_lock(&mutex);
// print out who is calling and how many times they called today
        printf("%s %d\n", s, i);
// sleep or wait a second for the president to process / handle the aftermath of the all
        sleep(1);

// unlock the mutex for next caller
        pthread_mutex_unlock(&mutex);
    }
// return null
    return NULL;
}
/*
* Main method to run program
*/
int main() {
// for loop to create threads or phone lines in this context for the president
    for (int i = 0; i < 4; ++i) {
// Create threads or phone lines for each country
        pthread_t russia, scotland, germany, norway;
// print line to seperate amount of call logs
        printf("================================\n");
// create thread for russia
        pthread_create(&russia, NULL, call, "Russia");
// create thread for scotland
        pthread_create(&scotland, NULL, call, "     Scotland");
// create thread for germany
        pthread_create(&germany, NULL, call, "            Germany");
// create thread for norway
        pthread_create(&norway, NULL, call, "                 Norway");
// join or set russia thread to null
        pthread_join(russia, NULL);
// join or set scotland thread to null
        pthread_join(scotland, NULL);
// join or set germany thread to null
        pthread_join(germany, NULL);
// join or set norway thread to null
        pthread_join(norway, NULL);
    }
// end program
    return 0;
}
