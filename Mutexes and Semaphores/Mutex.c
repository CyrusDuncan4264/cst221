// Cyrus Duncan CST 221 10/5/2020
// included librares
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

// thread lock and on and off bridge variables
pthread_mutex_t monitor_lock;
int onbridge = 0;
int offbridge = 0;

/*
* In this program we count the number of cars on a one lane bridge highway. We do this by first counting the number of   *  cars on the bridge in a for loop five times
*  if the number of cars on the bridge is 0 or less than 20 lock the thread and add the cars on the bridge
*  Afterwards we count the number of spaces open on the bridge and number of spaces not open on the bridge and unlock the *  thread. If the thread is locked we give an error message.
*/
void *on(){
	for (int i = 1; i < 5; i++) {
		if (onbridge == 0 || onbridge < 20) {
			pthread_mutex_lock(&monitor_lock);
			printf("The number of cars on the bridge is %d\n", onbridge);
			onbridge += 5;
            offbridge -= 5;
			printf("The number of cars on the bridge is now %d\n", onbridge);
			printf("Brdige access Thread Now Opened\n\n");
			pthread_mutex_unlock(&monitor_lock);
		} else {
			printf("The Bridge thread is locked %d\n", onbridge);
		}
	}
}
/*
*On the off thread see if the on bridge section is greator than 20 we lok the thread and subtract the number of cars getting off the bridge and add to the aviable spaces or cars getting on
* We then print the number of cars on the bridge
* and unlock the thread
*
*/
void *off() {
	for (int i = 1; i < 5; i++) {
		if (onbridge > 20) {
			pthread_mutex_lock(&monitor_lock);
			printf("The number of cars on the bridge is %d\n", onbridge);
			onbridge -= 5;
            offbridge += 5;
			printf("The number of cars on the bridge is now %d\n", onbridge);
			printf("Brdige access Thread Now Opened\n\n");
			pthread_mutex_unlock(&monitor_lock);
		} else {
			printf("The Bridge thread is locked %d\n", offbridge);
		}
	}
}

/*
*In the main we create two variables the rush1 and rush2 thread variables
* We then inizalize the monitor lock
* We create two rush trheads calling the on and off methods
* We join the threads nulling them or resetting them back to zero and print out the number of cars on and off the bridge
*/
int main() {
	pthread_t rush1, rush2;
	pthread_mutex_init(&monitor_lock, 0);
	pthread_create(&rush1, 0, on, 0);
	pthread_create(&rush2, 0, off, 0);

	printf("Starting threads\n");
	pthread_join(rush1, 0);
	pthread_join(rush2, 0);

	printf("After the number of cars on bridge is %d and number of cars off bridge is %d\n", onbridge, offbridge);

	return 0;
}
