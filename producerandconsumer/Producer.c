// Cyrus Duncan CST-221

#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

// Buffer Size
#define bufferSize 5

// Threads
pthread_mutex_t Buffer;
pthread_cond_t Wakeup, Sleep;

// variables
static int buffer[bufferSize];
int in = 0;
int out = 0;
int theProduct;


/* For put we must lock the stack and only allow access by the &Buffer thread.
* When the producer hits the buffer limit, it goes to sleep.
* If the buffer does not hit the buffer limit put the product in with one more counter on the index
* once the buffer size is bigger the consumer 
*/ Unlick the mutex lock allowing the consumer command to grab the buffer counter.
void put(int i) {
    // Lock the buffer
	pthread_mutex_lock(&Buffer);
    // if buffer is equal to max buffer size
	if (in == bufferSize) {
        // wait for the buffer to sleep
		pthread_cond_wait(&Sleep, &Buffer);
	}
    // set the buffer equal to parameter
	buffer[in] = i;
// increase the in index variable by one
	in += 1;
    // if index is greater than 0
	if(in > 0) {
            // wake the thread up
			pthread_cond_signal(&Wakeup);
	}
    // unlock the buffer thread
	pthread_mutex_unlock(&Buffer);
}

/* Aswell as the put action we lock the thread using mutex_lock
*  If buffer is equal to 0 or nothing but the thread to sleep
*  Once we take enough counter out of the Buffer thread and inIndex
* and outIndex both equal the bufferSize limit of 5,We restart the process wake up the
* producer to start filling the stack again. If consumer gets ahead of producer we put consumer to sleep
* After executing we unlock the mutex to create an onject buffer
*/
int get() {
    // lock the buffer thread
	pthread_mutex_lock(&Buffer);
    // if in index is equal to 0
	if (in == 0) {
       // wait for the buffer to wakeup
		pthread_cond_wait(&Wakeup, &Buffer);
	}
    // create a variable p equal to number of numbers coming out
	int p = buffer[out];
    // increase out variable by one
	out += 1;
    // if in index and out index is equal to max buffer size
	if (in == bufferSize && out == bufferSize) {
     // rest the variables
		in = 0;
		out = 0;
     // signal for thread sleep
		pthread_cond_signal(&Sleep);
	}
   // if out index is equal to in index 
	if(out == in) {
         // wait for the buffer thread to wakeup
			pthread_cond_wait(&Wakeup, &Buffer);
	}
   // unlock the buffer
	pthread_mutex_unlock(&Buffer);
    // return number of things leaving buffer.
	return p;
}

int produce() {
	theProduct++;
	printf("Produced: %i\n", theProduct);
	return theProduct;
}

void consume(int i){
	printf("Consumed: %i\n", i);
}

void *producer() {
	int i;
	while(1) {
		i = produce();
		put(i);
	}
}

void *consumer() {
	int i;
	while(1) {
		i = get();
		consume(i);
	}
}


//Main Function
int main(){
    // create thread variables pro for producer and con for consumer
	pthread_t con, pro;
   // inizialze the buffer thread
	pthread_mutex_init(&Buffer, 0);
   // inizalzie the wakeup thread
	pthread_cond_init(&Wakeup, 0);
  // inizalize the sleep thread
	pthread_cond_init(&Sleep, 0);
   // create thread for producer and pro thread variable both equal to 0
	pthread_create(&pro, 0, producer, 0);
    // create thread for consumer and pro thread variable both equal to 0
	pthread_create(&con, 0, consumer, 0);
// output variable, set thread variable to 0
	pthread_join(pro, 0);
 // output variable, set thread variable to 0
	pthread_join(con, 0);
   // return statement to end program
	return 0;
}

