#!/bin/bash

# Cyrus Duncan
# CST-221
# 11/15/2020
# Password Strength

# The purpose of this program is to read the end users password and notify the user if it requires refactoring
# This program checks to make sure of the following:
# The password has the minimum of 8 characters
# The password has at least one number
# The password must contain at least one special character.

# Count the number of tests the password passes
checks=0
#Read password from end user
read -p "Please enter a password to test and hit Enter: " password;
#Grab the password length
pwdLength=${#password}
#check if password length is less than 8
if (("$pwdLength" < 8)); then
#If it is not print an error
	echo 'Your password should be at least 8 characters long.'
else
#if it is ass a pass to this test
	let "checks++"
#end if else check
fi
#Check to see if password has a number in it
if [[ $password =~ [0-9] ]]; then
#If it does add a pass to the check
	let "checks++"
else
#Otherwise print an error stating that we must have a number in the password
	echo 'Your password should have at least one number.'
	
#End if else
fi
#Finally check if the password has a special charter
if [[ $password != *['!'@#\$%^\&*()_+]* ]]; then
#If not print an erorr stating that it should have at least one special character
	echo 'Your password should contain at least one special character.'
else
#If so add a pass to the check counter
	let "checks++"
#End if else
fi
#Check if the passed tests is equal to 3
if [ "$checks" == "3" ]; then
	echo 'Your password is secure!'
#End if statement
fi
