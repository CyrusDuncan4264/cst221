#!/bin/bash

# Author: Cyrus Duncan
# Course: CST - 221
#  Date: 11/5/2020

# This application greats a group
# Then reads from a file passed in to the bash application called users.txt
# It reads through users text 

#Create a Group name
group="CST221"
#if the group already exists in the /etc/group folder
if grep -q $group /etc/group
then
#print an error stating the group already exists
     echo "group exists"
else
#else add the group CST221
     groupadd $group
#end the if else statement
fi

#Create filename variable eqial to users.txt
filename="users.txt"
# Read while there is not an EOF passing through username and password
while read -r username password
do
{
# Create variable named name from the username
    	usr="$username"
# Create variable named pass from the pwd
	pwd="$password"
# print out the Username
	echo "Username: $usr"
# print out the Password
	echo "Password: $pwd"
#if the id is unique and equal to 0 
    	if [ $(id -u) == 0 ]; then
# search for user in etc/passwd
		egrep "^$usr" /etc/passwd >/dev/null
# if the last called egrep command is equal to zero
		if [ $? == 0 ]; then
# Print password already exists
			echo "$pwd exists!"
# Close the bash program
			exit 1
# If nothing is found
		else
# add the user with the username and password
			useradd -m -p $usr $pwd
# if the last command was executed and print user has been added to system or it is equal to 0 print failed to add the user 
			[ $? == 0 ] && echo "User has been added!" || echo "Failed to add the  user!"
# end the inner if statement
		fi
# if the program does not execute
	else
# print that only the root user can add users to the system
		echo "Only root may add users to the system"
# exit the bash application
		exit 2
#end the nested if statement
	fi
#Modify the user to add the user to the group
	usermod -aG $group $pwd
}
#close the file with filename
done < "$filename"

#open the file users.txt
filename="users.txt"
#read each line with username and password
while read -r username password
do
#parse the username variable to usr
	usr="$username"
#parse the password to pwd variable
	pwd="$password"
#remove user with the same username
	userdel -r $usr
	echo "User Deleted."
#close the users.txt file
done < "$filename"

# if the group exists
if grep -q $group /etc/group
then
# delete the group
	groupdel $group
else
# otherwise print the group
     	echo "Group does not exist."
#close if statement
fi

